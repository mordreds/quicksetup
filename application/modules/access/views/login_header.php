<!DOCTYPE html>
<html lang="en">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>resources/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>resources/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>resources/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>resources/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>resources/css/colors.min.css" rel="stylesheet" type="text/css">

    <link href="<?=base_url()?>resources/images/favicon.ico" rel="icon" type="image/png" sizes="16x16">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?=base_url()?>resources/js/main/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>resources/js/main/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>resources/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>resources/js/plugins/ui/ripple.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="<?=base_url()?>resources/js/plugins/forms/validation/validate.min.js"></script>
    <script src="<?=base_url()?>resources/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="<?=base_url()?>resources/js/app.js"></script>
    <script src="<?=base_url()?>resources/js/demo_pages/login_validation.js"></script>
    <!-- /theme JS files -->

    <!-- Added JS Files -->
    <script type="text/javascript" src="<?=base_url()?>resources/js/plugins/notifications/bootbox.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>resources/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>resources/js/demo_pages/components_modals.js"></script>

  </head>

  <body>