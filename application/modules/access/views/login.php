<body class="bg-slate-800">

  <!-- Page content -->
  <div class="page-content login-cover">

    <!-- Main content -->
    <div class="content-wrapper">

      <!-- Content area -->
      <div class="content d-flex justify-content-center align-items-center">

        <!-- Login card -->
        <form class="login-form form-validate" method="post" action="<?=base_url()?>dashboard">
          <div class="card mb-0">
            <div class="card-body">
              <div class="text-center mb-3">
                <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                <h5 class="mb-0">Login to your account</h5>
                <span class="d-block text-muted">Your credentials</span>
              </div>

              <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="text" class="form-control" name="username" placeholder="Username" oncopy="return false;" onpaste="return false;" onselectstart="return false;" autocomplete="off" required>
                <div class="form-control-feedback">
                  <i class="icon-user text-muted"></i>
                </div>
              </div>

              <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="password" class="form-control" name="password" placeholder="Password" oncopy="return false;" onpaste="return false;" onselectstart="return false;" autocomplete="off" required>
                <div class="form-control-feedback">
                  <i class="icon-lock2 text-muted"></i>
                </div>
              </div>

              <div class="form-group d-flex align-items-center">
                <div class="form-check mb-0">
                  <label class="form-check-label">
                    <input type="checkbox" name="remember" class="form-input-styled" checked data-fouc>
                    Remember
                  </label>
                </div>

                <a href="login_password_recover.html" class="ml-auto">Forgot password?</a>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
              </div>
            </div>
          </div>
        </form>
        <!-- /login card -->

      </div>
      <!-- /content area -->


      <!-- Footer -->
      <div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
          <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
          </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
          <span class="navbar-text p-0">
            &copy; 2018 - 2019. <a href="#"><em>Okomfo Anokye Rural Bank</em> | </a> <em>Powered by <a href="https://marksbon.com" target="_blank">Marksbon Limited</a></em>
          </span>

          <ul class="navbar-nav ml-lg-auto">
            <li class="nav-item"><a href="https://tasksimino.marksbon.com?ref=appname&comp=name" class="navbar-nav-link" target="_blank"  style="padding: .4rem 1rem"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
            <li class="nav-item"><a href="https://license.marksbon.com?ref=appname&comp=name&client=id" class="navbar-nav-link font-weight-semibold" target="_blank" style="padding: .4rem 1rem"><span class="text-pink-400"><i class="icon-file-text2 mr-2"></i> License</span></a></li>
          </ul>
        </div>
      </div>
      <!-- /footer -->

    </div>
    <!-- /main content -->

  </div>
  <!-- /page content -->

</body>